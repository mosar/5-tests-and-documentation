# -*- coding: utf-8 -*-
"""Functions for getting user inputs in the mytestdocs package

Author
------
Jenni Rinker
rink@dtu.dk
"""


def get_int():
    """Get an integer from the user

    Notes
    -----
    The function will raise an error if the user does not specify an integer.

    Returns
    -------
    x : int
        A specified integer.
    """
    x = input('Give me an integer: ')  # get user input as a string
    try:  # try to convert x to an integer
        x = int(x)  # convert the string to an int
    except ValueError:  # if we cannot convert it, raise an error
        raise ValueError('You did not give me an integer!')
    return x


def get_float():
    """Get a float from the user

    Notes
    -----
    The function will convert an integer to a float or raise an error if the
    user does not specify an int or float.

    Returns
    -------
    x : int, float
        A specified float or integer.
    """
    x = input('Give me a float: ')  # get user input as a string
    x = float(x)  # convert to a float
    return x


def get_str():
    """Get a string from the user

    Notes
    -----
    The function will convert all input to a string.

    Returns
    -------
    x : any
        User input.
    """
    x = input('Give me a string: ')  # get user input as a string
    return x
