# Workshop 5: Test-Driven Development and Documentation

This is the fifth in a series of scientific Python lectures at Risø campus,
Denmark Technical University.

## Workshop objective

To make users more comfortable with writing tests to verify that their code is
correct and how to write documentation to help other users understand your code
better.

## Who should come

Anyone interested in creating code collaboratively and sharing with your 
colleagues. Previous Python experience not required.

## Date

The workshop date and location will be announced internally at DTU Risø. Please
use the contact information below for questions on the workshop contents or 
arranging a new workshop.

## Topic outline

- What is test-driven development?
- Python packages for testing
- Exercise: write a test
- Documentation: why bother?
- Documentation conventions
- Python packages for automatic documentation

## Prerequisites

If you are attending the workshop, please do the following before attending:
1. If you do not have Anaconda installed, please [install it](https://www.anaconda.com/download/)
**with Python 3.6**
2. If you have Anaconda installed, please either  
    a) have your root environment be Python 3.6, or  
    b) [create an environment](https://conda.io/docs/user-guide/tasks/manage-environments.html#creating-an-environment-with-commands)
    that has Python 3.6
3. Use `conda` to install the `sphinx`,  `pytest`, and `coverage` packages
4. Fork this repository to your own account, and clone the fork to your computer

## Contact

Jenni Rinker  
rink@dtu.dk